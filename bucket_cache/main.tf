resource "google_storage_bucket" "bucket" {
  name          = var.bucket_name
  project       = var.project
  location      = var.location 
  storage_class = var.storage_class
  uniform_bucket_level_access = var.uniform_bucket_level_access
  public_access_prevention = var.public_access_prevention

  versioning {
    enabled     = var.versioning_enable
  }

  lifecycle_rule {
    action {
      type = var.lifecycle_type
    }
    condition {
      age = var.lifecycle_age
    }
  }

  force_destroy = var.force_destroy
}

