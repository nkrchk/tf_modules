variable "bucket_name" {
  type = string
}

variable "project" {
  type = string
}

variable "location" {
  type = string
}

variable "storage_class" {
  type = string
}

variable "uniform_bucket_level_access" {
  type = bool
}

variable "public_access_prevention" {
  type = string
}

variable "versioning_enable" {
  type = bool
}

variable "lifecycle_type" {
  type = string
}

variable "lifecycle_age" {
  type = string
}

variable "force_destroy" {
  type = bool
  default = true
}