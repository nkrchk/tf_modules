resource "google_compute_subnetwork" "private" {
  name                     = var.privatenet_name
  ip_cidr_range            = var.ip_cidr_range 
  region                   = var.region 
  network                  = var.private_network
  private_ip_google_access = var.private_ip_google_access

  secondary_ip_range {
    range_name    = var.firstiprange_name
    ip_cidr_range = var.firstiprange
  }
  secondary_ip_range {
    range_name    = var.secondiprange_name
    ip_cidr_range = var.secondiprange
  }
}
