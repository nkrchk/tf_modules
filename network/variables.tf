variable "privatenet_name" {
  type = string
}

variable "ip_cidr_range" {
  type = string
}

variable "region" {
  type = string
}

variable "private_network" {
  type = string
}

variable "private_ip_google_access" {
  type = bool
}

variable "firstiprange_name" {
  type = string
}

variable "firstiprange" {
  type = string
}

variable "secondiprange_name" {
  type = string
}

variable "secondiprange" {
  type = string
}