output "subnetwork_private_id" {
  value = google_compute_subnetwork.private.id
}

output "subnetwork_private_self_link" {
  value = google_compute_subnetwork.private.self_link
}