resource "google_compute_router_nat" "nat" {
  name   = var.nat_name
  router = var.router_name
  region = var.region
  nat_ips = [var.nat_ips]

  source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip
  nat_ip_allocate_option             = var.nat_ip_allocate

  subnetwork {
    name                    = var.subnetwork_name
    source_ip_ranges_to_nat = [var.subnet_source_ip_ranges]
  }
}