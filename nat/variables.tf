variable "nat_name" {
  type = string
}

variable "router_name" {
  type = string
}

variable "region" {
  type = string
}

variable "source_subnetwork_ip" {
  type = string
}

variable "nat_ip_allocate" {
  type = string
}

variable "subnetwork_name" {
  type = string
}

variable "subnet_source_ip_ranges" {
  type = string
}

variable "nat_ips" {
  type = string
}