resource "google_sql_database_instance" "instance" {
  name             = var.instance_name
  region           = var.region
  database_version = var.db_version
  settings {
    tier = var.db_tier
    ip_configuration {
      ipv4_enabled    = var.ipv4_enabled
      private_network = var.vpc_id
    }
  }

  deletion_protection  = "false"
}

resource "google_sql_database" "database" {
  name     = var.db_name
  instance = google_sql_database_instance.instance.name
  charset = var.charset
  collation = var.collation
}

resource "google_sql_user" "users" {
  name     = var.db_user
  instance = google_sql_database_instance.instance.name
  host     = var.allow_host
  password = var.db_password
}
