variable "instance_name" {
  type = string
}

variable "region" {
  type = string
}

variable "db_version" {
  type = string
}

variable "db_tier" {
  type = string
}

variable "ipv4_enabled" {
  type = bool
}

variable "vpc_id" {
  type = string
}

variable "db_name" {
  type = string
}

variable "db_user" {
  type = string
}

variable "allow_host" {
  type = string
}

variable "db_password" {
  type = string
}

variable "charset" {
  type = string
  default = "utf8mb4"
}

variable "collation" {
  type = string
  default = "utf8mb4_0900_ai_ci"
}

