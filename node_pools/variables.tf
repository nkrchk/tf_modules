variable "nodepool_name" {
  type = string
}

variable "cluster_id" {
  type = string
}

variable "node_count" {
  type = string
}

variable "auto_repair" {
  type = bool
}

variable "auto_upgrade" {
  type = bool
}

variable "machine_type" {
  type = string
}

variable "preemptible" {
  type = bool
}

variable "role_name" {
  type = string
}

variable "service_account" {
  type = string
}

variable "oauth_scopes" {
  type = string
}

variable "disk_size" {
  type = string
}