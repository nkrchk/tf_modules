resource "google_container_node_pool" "general" {
  name       = var.nodepool_name
  cluster    = var.cluster_id
  node_count = var.node_count

  management {
    auto_repair  = var.auto_repair
    auto_upgrade = var.auto_upgrade
  }

  node_config {
    preemptible  = var.preemptible
    machine_type = var.machine_type
    disk_size_gb = var.disk_size

    labels = {
      role = var.role_name
    }

    service_account = var.service_account
    oauth_scopes = [
      var.oauth_scopes
    ]
  }
}
