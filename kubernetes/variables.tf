variable "cluster_name" {
  type = string
}

variable "location" {
  type = string
}

variable "remove_default_node_pool" {
  type = bool
}

variable "node_count" {
  type = string
}

variable "network" {
  type = string
}

variable "subnetwork" {
  type = string
}

variable "logging_service" {
  type = string
}

variable "monitoring_service" {
  type = string
}

variable "networking_mode" {
  type = string
}

variable "second_node_location" {
  type = string
}

variable "disable_http_load_balancing" {
  type = bool
}

variable "disable_horiz_pod_autoscaling" {
  type = bool
}

variable "release_channel" {
  type = string
}

variable "cluster_secondary_range_name" {
  type = string
}

variable "services_secondary_range_name" {
  type = string
}

variable "enable_private_nodes" {
  type = bool
}

variable "enable_private_endpoint" {
  type = bool
}

variable "master_ipv4_cidr_block" {
  type = string
}