resource "google_container_cluster" "primary" {
  name                     = var.cluster_name
  location                 = var.location
  remove_default_node_pool = var.remove_default_node_pool
  initial_node_count       = var.node_count
  network                  = var.network
  subnetwork               = var.subnetwork 
  logging_service          = var.logging_service 
  monitoring_service       = var.monitoring_service 
  networking_mode          = var.networking_mode 

  # Optional, if you want multi-zonal cluster
  node_locations = [
    var.second_node_location
  ]

  addons_config {
    http_load_balancing {
      disabled = var.disable_http_load_balancing
    }
    horizontal_pod_autoscaling {
      disabled = var.disable_horiz_pod_autoscaling
    }
  }

  release_channel {
    channel = var.release_channel
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = var.cluster_secondary_range_name
    services_secondary_range_name = var.services_secondary_range_name
  }

  private_cluster_config {
    enable_private_nodes    = var.enable_private_nodes 
    enable_private_endpoint = var.enable_private_endpoint
    master_ipv4_cidr_block  = var.master_ipv4_cidr_block
  }
}
