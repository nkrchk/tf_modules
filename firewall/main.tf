resource "google_compute_firewall" "allow-ssh" {
  name    = var.firewall_name
  network = var.ssh_network

  allow {
    protocol = var.protocol
    ports    = [var.ports]
  }

  source_ranges = [var.ranges]
}
