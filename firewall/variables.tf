variable "firewall_name" {
  type = string
}

variable "ssh_network" {
  type = string
}

variable "protocol" {
  type = string
}

variable "ports" {
  type = string
}

variable "ranges" {
  type = string
}